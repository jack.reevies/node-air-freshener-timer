const Server = require('./server');
const fs = require('fs')
const fetch = require('node-fetch')

const schedule = { on: 15, off: 45 }
const devices = [

]

const state = {
  current: 'off',
  nextSwitchAt: new Date(),
  activeTimeout: undefined
}

function saveState() {
  const json = JSON.stringify({ schedule, devices, state: { ...state, activeTimeout: undefined } })
  fs.writeFileSync('state.json', json)
}

function restoreState() {
  const json = fs.readFileSync('state.json', 'utf8')
  const data = JSON.parse(json)
  schedule.on = data.schedule.on || schedule.on
  schedule.off = data.schedule.off || schedule.off
  devices.length = 0
  for (const device of data.devices) {
    if (!devices.includes(device)) {
      devices.push(device)
    }
  }
  state.current = data.state.current || state.current
  state.nextSwitchAt = data.state.nextSwitchAt || state.nextSwitchAt
  state.activeTimeout = undefined
}

async function start() {
  try { restoreState() } catch (e) { }
  setInterval(saveState, 1000 * 60 * 5)
  const server = Server.start(process.env.PORT || 3000)

  server.use(async (req, res) => {
    log(`${req.method} ${req.url}`)
    return true
  })

  server.get('/', (req, res) => {
    res.sendJson({
      schedule,
      devices,
      state: {
        ...state, nextSwitchAt: new Date(state.nextSwitchAt).toString(), timeLeft: Math.round((state.nextSwitchAt - Date.now()) / 60_000), activeTimeout: undefined
      }
    })
  })

  server.get('/schedule', (req, res) => {
    res.sendJson(schedule)
  })

  server.post('/restart', async (req, res) => {
    turnOn()
    res.sendJson({ message: 'OK!' })
  })

  server.post('/on', async (req, res) => {
    turnOn()
    res.sendJson({ message: 'OK!' })
  })

  server.post('/off', async (req, res) => {
    turnOff()
    res.sendJson({ message: 'OK!' })
  })

  server.put('/schedule', async (req, res) => {
    const json = await req.json()
    schedule.on = json.on || schedule.on
    schedule.off = json.off || schedule.off
    res.sendJson(schedule)
  })

  server.get('/devices', (req, res) => {
    res.sendJson(devices)
  })

  server.patch('/devices', async (req, res) => {
    const json = await req.json()
    for (const device of json) {
      if (!devices.includes(device)) {
        devices.push(device)
      }
    }
    res.sendJson(devices)
  })

  server.put('/devices', async (req, res) => {
    const json = await req.json()
    devices.length = 0
    for (const device of json) {
      if (!devices.includes(device)) {
        devices.push(device)
      }
    }
    res.sendJson(devices)
  })

  server.delete('/devices', async (req, res) => {
    const json = await req.json()
    for (const device of json) {
      const index = devices.indexOf(device)
      if (index > -1) {
        devices.splice(index, 1)
      }
    }
    res.sendJson(devices)
  })

  turnOn()
}

function turnOn() {
  if (state.activeTimeout) {
    clearTimeout(state.activeTimeout)
  }

  devices.map(o => setAntelaDevice(o, "on"))
  state.current = 'on'
  log(`Turned devices on for ${schedule.on} minutes`)
  state.nextSwitchAt = Date.now() + 1000 * 60 * schedule.on

  state.activeTimeout = setTimeout(turnOff, 1000 * 60 * schedule.on)
}

function turnOff() {
  if (state.activeTimeout) {
    clearTimeout(state.activeTimeout)
  }

  devices.map(o => setAntelaDevice(o, "off"))
  state.current = 'off'
  log(`Turned devices off for ${schedule.off} minutes`)
  state.nextSwitchAt = Date.now() + 1000 * 60 * schedule.off

  state.activeTimeout = setTimeout(turnOn, 1000 * 60 * schedule.off)
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

async function setAntelaDevice(alias, state) {
  try {
    const res = await fetch(`${process.env.ANTELA_URL}/${alias}/${state}`, { method: 'PUT' })
    const text = await res.text()
    log(text)
    log(`Changed ${alias} override state to ${state}`)
  } catch (e) {
    log(`Failed to setAntelaDevice ${e}`)
  }
}

function log(msg) {
  console.log(`${new Date().toLocaleString()} | ${msg}`)
}

start()