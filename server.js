const http = require('http');

function start(port) {
  const httpServer = http.createServer();
  const handlers = []
  const middleware = []

  httpServer.listen(port, () => {
    console.log(`\x1b[32m%s\x1b[0m`, `Listening on port ${port}`);
  });

  httpServer.on('request', async (req, res) => {
    req.json = () => readJsonBody(req)
    req.text = () => readBody(req)
    res.sendJson = (json) => sendJson(res, json);

    for (const handler of middleware) {
      if (!await handler(req, res)) {
        return
      }
    }
    for (const handler of handlers) {
      if (!await handler(req, res)) {
        return
      }
    }
  });

  function sendJson(res, json) {
    const jsonStr = JSON.stringify(json);
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Content-Length', jsonStr.length)
    res.write(jsonStr);
    res.end();
  }

  async function readJsonBody(req) {
    const text = await readBody(req)
    return JSON.parse(text)
  }

  function readBody(req) {
    return new Promise((resolve, reject) => {
      let body = '';
      req.on('data', (chunk) => {
        body += chunk;
      });
      req.on('end', () => {
        resolve(body);
      });
      req.on('error', (err) => {
        reject(err);
      });
    });
  }

  function registerPath(path, method, handler) {
    handlers.push((req, res) => {
      if (req.url === path && req.method.toLowerCase() === method.toLowerCase()) {
        return handler(req, res);
      }
      return true
    })
  }

  return {
    use: (handler) => middleware.push(handler),
    get: (path, handler) => registerPath(path, 'get', handler),
    post: (path, handler) => registerPath(path, 'post', handler),
    patch: (path, handler) => registerPath(path, 'patch', handler),
    put: (path, handler) => registerPath(path, 'put', handler),
    delete: (path, handler) => registerPath(path, 'delete', handler),
  }
}

module.exports = { start }